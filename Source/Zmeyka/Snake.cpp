// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementsSize = 10.f;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	MovementSpeed = 200.f;
	LastMoveDirection;
}

void ASnake::AddSnakeElement(int ElementsNum)
{
	for(int i = 0; i < ElementsNum; ++i) 
	{
		FVector NewLocation(SnakeElements.Num() * ElementsSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if(ElemIndex == 0) 
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovmentSpeed = ElementsSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovmentSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovmentSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovmentSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovmentSpeed;
		break;
	}

	AddActorWorldOffset(MovementVector);


	for (int i = SnakeElements.Num() - 1; i > 0; i--) 
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
}



